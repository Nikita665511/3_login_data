const fs = require('fs');
const { promisify } = require('util');

const myPromisify = (fn) => {
      return new Promise((resolve, reject) => {
        fs.writeFile('file1.txt','utf-8',(error)=> {
        if (error) {
           reject(error)
         }else {
           resolve("File created successfully");
          }
        })
        return new Promise((resolve,reject)=>{
            fs.writeFile('file2.txt','utf-8',(error)=>{
                if(error){
                    reject(error)
                }else{
                    resolve("Another file is created successfully")
                }
             })
        setTimeout(()=> {
            return new Promise((resolve,reject)=>{
                fs.unlink('file1.txt','utf-8',(error)=>{
                    if(error){
                        reject(error)
                    }
                    else{
                        resolve("file1 is deleted successfully")
                    }
                })
            })
        })
        return new Promise((resolve,reject)=>{
            fs.unlink('file2.txt','utf-8',(error)=>{
                if(error){
                    reject(error)
                }else{
                    resolve("file2 is deleted successfully")
                }
            })
        })
   
      },2000);
    
    })
}
  
function problem2 (fileName){
    promiseReadFile('lipsum.txt')
    .then((data)=>{
        console.log("Reading file done");
        return promiseWriteFile(fileName,data)
    })
    .then(()=>{
        console.log("Writing file done");
        return promiseDeleteFile('lipsum.txt');
    })
    .then(()=>console.log("original file deleted"))
    .catch((error)=>console.error(error));
}

problem2('file3.txt');